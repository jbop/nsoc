%union {
    int exit_code;
    char *str;
    long lng;
    double dbl;
}

%token INTEGER
%token IDENTIFIER
%token STRING
%token FLOAT

%type <exit_code> value
%type <exit_code> dict

%start decls

%pure-parser
%lex-param {void * scanner}
%parse-param {void * scanner}

%{
#include <stdio.h>

/* From nsoc.lex.c */
int nsoclex ( YYSTYPE * lvalp, void *scanner );

/* Error handler */
static void nsocerror(void *scanner, const char *msg);

%}
%%

value: INTEGER {
	printf("Got int\n");
}

value: FLOAT {
	printf("Got float\n");
}

value: STRING {
	printf("Got string\n");
}

value: dicts {
	printf("Got dict\n");
}

dicts: dict {
	printf("Single dict\n");
}

dicts: dicts dict {
	printf("Merging dicts\n");
}

dict: '#' STRING {
	printf("Include\n");
}

dict: '@' path {
	printf("Referenced template\n");
}

dict: '{' '}' {
	printf("Empty dict\n");
}
dict: '{' decls '}' {
	printf("Non-empty dict\n");
}

decls: IDENTIFIER '=' value {
	printf("decl start\n");
}
decls: decls ',' IDENTIFIER '=' value {
	printf("decl fill\n");
}

path: IDENTIFIER {
	printf("Path part\n");
}

path: path '.' IDENTIFIER {
	printf("Part continuation\n");
}

%%

static void nsocerror(void *scanner, const char *msg) {
	printf("Error: %s\n", msg);
}
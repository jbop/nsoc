/*
 * main.c
 *
 *  Created on: Feb 11, 2014
 *      Author: msikstrom
 */

#include "nsoc.tab.h"
#include <stdio.h>

int nsocparse(void *scanner);
int nsoclex_init(void **scanner);
int nsoclex_set_in(void *scanner, FILE *input);
int nsoclex_destroy(void *scanner);

int main( int argc, char *argv[] ) {
	void *scanner;
	FILE *input = NULL;

	if( argc == 2 ) {
		input = fopen(argv[1], "r");
		if(!input) {
			fprintf(stderr, "Error opening file %s\n", argv[2]);
			return 1;
		}
	}

	nsoclex_init(&scanner);
	nsoclex_set_in(scanner, input ? input : stdin);
	nsocparse(scanner);
	nsoclex_destroy(scanner);
	if(input) {
		fclose(input);
	}
	return 0;
}

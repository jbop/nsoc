%{
#include "nsoc.tab.h"
#include <stdio.h>
#define YY_NO_INPUT

int nsoclex_set_in( void *scanner, FILE *input );

%}

%option nounput
%option bison-bridge
%option reentrant

%%

[0-9]+    {
	printf("Int: >%s<\n", yytext);
	return INTEGER;
}

[0-9]+"."[0-9]* {
	printf("Float: >%s<\n", yytext);
	return INTEGER;
}

[a-z][a-z0-9]* {
	printf("Id: >%s<\n", yytext);
	return IDENTIFIER;
}

\"(\\.|[^"\\])*\" {
	printf("String: >%s<\n", yytext);
	return STRING;
}

[ \t\n]+ /* Whitespace, be gone */

. { return *yytext; }

%%


int nsocwrap( void *yyscanner ) {
	/* we never wrap around... */
	return 1;
}

int nsoclex_set_in( void *yyscanner, FILE *input ) {
    struct yyguts_t * yyg = (struct yyguts_t*)yyscanner;
    yyin = input;
    return 0;
}